class ArrayNumberSum

  # Bruteforce search for 2 number that sum target
  def two_number_sum(array, target_sum)
    # Traverse-1
    (0...array.length).each do |idx|
      first_num = array[idx]
      # Traverse-2
      (idx+1...array.length).each do |jdx|
        second_num = array[jdx]

        if first_num + second_num == target_sum
          return [first_num, second_num]
        end

      end
    end
  end

  def two_number_sum_hash(array, target_sum)
    nums = {}
    array.each do |num|
      required_number = target_sum - num
      if nums.key? required_number
        return [required_number, num]
      else
        nums[num] = true
      end
    end
    []
  end

  def two_number_sum_pointers(array, target_sum)
    array.sort!
    left = 0
    right = array.length - 1

    while left < right
      current_sum = array[left] + array[right]
      if current_sum == target_sum
        return [array[left], array[right]]
      elsif current_sum < target_sum
        left += 1
      elsif current_sum > target_sum
        right -= 1
      end
    end
    []
  end

end