class LongestPalindromicSubstring
  def find_longest(string)
    current_longest = [0,1]
    # cant expand on left letter, skip 0
    (1..string.length).each do |idx|
      left_side = idx - 1
      right_side = idx + 1
      middle = idx

      # get 2 index pointers
      ## Odd: aba
      odd = get_longest_palindrome_from(string, left_side, right_side)
      ## Even: xzzx
      even = get_longest_palindrome_from(string, left_side, middle)
      # longest of 2 palindromes, compare disntances
      longest = [odd, even].max_by { |x| x[1] - x[0] }
      current_longest = [longest, current_longest].max_by { |x| x[1] - x[0]}
    end
    # slice substring, dont count last index
    string[current_longest[0]...current_longest[1]]
  end

  def get_longest_palindrome_from(string, left_idx, right_idx)
    # keep inside string, dont go outside of it
    while left_idx >= 0 && right_idx < string.length
      # Core: different breaks the palindrome
      break if string[left_idx] != string[right_idx]

      left_idx -= 1
      right_idx += 1
    end
    [left_idx + 1, right_idx]
  end
end