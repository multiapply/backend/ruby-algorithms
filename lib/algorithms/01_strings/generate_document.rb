require 'set'

class GenerateDocument

  def validate_per_char(shuffle_chars, document)
    document.each_char do |character|
      document_freq = count_character_frequency(character, document)
      shuffle_chars_freq = count_character_frequency(character, shuffle_chars)

      return false if document_freq > shuffle_chars_freq
    end
    true
  end

  def validate_per_char_unique(shuffle_chars, document)
    already_counted = Set.new
    document.each_char do |character|
      next if already_counted.include? character

      document_freq = count_character_frequency(character, document)
      shuffle_chars_freq = count_character_frequency(character, shuffle_chars)

      return false if document_freq > shuffle_chars_freq
      already_counted << character
    end
    true
  end

  def count_character_frequency(character, target)
    frequency = 0
    target.each_char do |char|
      frequency += 1 if char == character
    end
    frequency
  end

  def validate_with_hash(shuffle_chars, document)
    char_counts = Hash.new(0)
    shuffle_chars.each_char do |character|
      char_counts[character] += 1
    end
  
    document.each_char do |character|
      return false if char_counts[character] == 0
      char_counts[character] -= 1
    end
    true
  end

  def randomize_string(string)
    string.chars.shuffle.join
  end
end