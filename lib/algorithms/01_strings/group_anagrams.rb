class GroupAnagrams
  def group(words)
    return [] if words.empty?
    sorted_letters = words.map { |w| w.chars.sort.join }
    alpha_indices = (0...words.length).to_a
    alpha_indices.sort_by! { |x| sorted_letters[x]}

    result = []
    result_group = []

    sorted_base = sorted_letters[alpha_indices[0]]
    alpha_indices.each do |index|
      # group
      word = words[index]
      sorted_word = sorted_letters[index]

      if sorted_word == sorted_base
        result_group.push(word)
        next
      end
      # next group
      result.push(result_group)
      
      result_group = [word]
      sorted_base = sorted_word
    end
    result.push(result_group)
    result
  end
end