class FirstNonRepeatChar

  def first_non_repeating_character(string)
    string.each_char.with_index do |char1, idx1|
      found_duplicate = false

      string.each_char.with_index do |char2, idx2|
        if char1 == char2 && idx1 != idx2
          found_duplicate = true
          break
        end
      end

      return idx1 unless found_duplicate
    end
    -1
  end

  def first_non_repeating_char_hash(string)
    character_frequencies = Hash.new(0)
    
    string.each_char do |character|
      character_frequencies[character] += 1
    end
  
    string.each_char.with_index do |character, idx|
      return idx if character_frequencies[character] == 1
    end
    -1
  end

end