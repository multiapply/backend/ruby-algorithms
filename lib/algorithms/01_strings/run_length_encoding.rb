class RunLengthEncoding

  def encode_str(string)
    encoded_res = []
    current_run_length = 1

    (1..string.length).each do |idx|
      current_character = string[idx]
      previous_character = string[idx - 1]

      if current_character != previous_character || current_run_length == 9
        encoded_res.push(current_run_length.to_s)
        encoded_res.push(previous_character)
        current_run_length = 0
      end
      current_run_length += 1
    end

    # encoded_res.push(current_run_length.to_s)
    # encoded_res.push(string[-1])

    encoded_res.join('')

  end
end