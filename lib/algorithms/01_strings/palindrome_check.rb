class PalindromeCheck
  def check(string)
    reversed_string = ""
    string.reverse.each_char do |char|
      reversed_string += char
    end

    string == reversed_string
  end

  def check_with_arr(string)
    reversed_string = []
    string.reverse.each_char do |char|
      reversed_string << char
    end

    string == reversed_string.join("")
  end

  def check_with_recur(string, first_idx = 0)
    last_idx = string.length - 1 - first_idx

    if first_idx >= last_idx
      # get into middle
      return true
    else
      return string[first_idx] == string[last_idx] && 
          check_with_recur(string, first_idx + 1)
    end
  end

  def check_with_tail_recur(string, first_idx = 0)
    last_idx = string.length - 1 - first_idx

    return true if first_idx >= last_idx
    return false unless string[first_idx] == string[last_idx]
    return check_with_recur(string, first_idx + 1)
  end

  def check_with_pointers(string)
    left_idx = 0
    right_idx = string.length - 1

    while left_idx < right_idx
      return false unless string[left_idx] == string[right_idx]
      left_idx += 1
      right_idx -= 1
    end
    true
  end

end