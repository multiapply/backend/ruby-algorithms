class CaesarCipher
  def encrypt(string, key)
    new_letters = []
    new_key = key % 26

    string.each_char do |letter|
      new_letters << get_new_letter(letter, new_key)
    end

    new_letters.join
  end
  
  def get_new_letter(letter, key)
    alpha_start = 96
    letter_z = 122
    
    new_letter_code = letter.ord + key

    if new_letter_code <= letter_z
      new_letter_code.chr
    else
      (alpha_start + new_letter_code % letter_z).chr
    end
  end
  
end