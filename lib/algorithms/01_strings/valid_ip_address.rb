class ValidIPAddress
  def generate_valid_addresses(string)
    ip_addresses_found = []
    first_value_end = [string.length,4].min
    (1...first_value_end).each do |idx_first|
      current_ip_address_parts = ['','','','']

      current_ip_address_parts[0] = string[0...idx_first]
      next unless is_valid_part(current_ip_address_parts[0])

      second_value = idx_first + 1
      second_value_end = idx_first + [string.length - idx_first, 4].min
      (second_value...second_value_end).each do |idx_second|

        current_ip_address_parts[1] = string[idx_first...idx_second]
        next unless is_valid_part(current_ip_address_parts[1])

        third_value = idx_second + 1
        third_value_end = idx_second + [string.length - idx_second, 4].min
        (third_value...third_value_end).each do |idx_third|

          current_ip_address_parts[2] = string[idx_second...idx_third]
          current_ip_address_parts[3] = string[idx_third..-1]

          if is_valid_part(current_ip_address_parts[2]) && is_valid_part(current_ip_address_parts[3])
            ip_addresses_found.push(current_ip_address_parts.join('.'))
          end
        end
      end
    end
    ip_addresses_found
  end

  def is_valid_part(string)
    string_as_int = string.to_i
    return false if string_as_int > 255

    # check for leading 0
    string.length == string_as_int.to_s.length
  end
end
