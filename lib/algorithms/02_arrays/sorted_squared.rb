class SortedSquared
  def sort_and_square(array)
    sorted_squares = Array.new(array.length, 0)
    smaller_value_idx = 0
    larger_value_idx = array.length - 1

    (array.length - 1).downto(0) do |idx|
      smaller_value = array[smaller_value_idx]
      larger_value = array[larger_value_idx]
      if smaller_value.abs > larger_value.abs
        sorted_squares[idx] = smaller_value * smaller_value
        smaller_value_idx += 1
      else
        sorted_squares[idx] = larger_value * larger_value
        larger_value_idx -= 1
      end
    end
    sorted_squares

  end
end