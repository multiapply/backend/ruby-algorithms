class ValidateSubsequence
  def validate(array, sequence)
    arr_idx = 0 # move_fast
    seq_idx = 0 # move_slow

    while arr_idx < array.length && seq_idx < sequence.length
      if array[arr_idx] == sequence[seq_idx]
        seq_idx += 1
      end
      arr_idx += 1
    end

    seq_idx == sequence.length
  end

  def validate_with_for(array, sequence)
    seq_idx = 0
    array.each do |value|
      break if seq_idx == sequence.length
      
      if sequence[seq_idx] == value
        seq_idx += 1
      end
    end
    seq_idx == sequence.length
  end
end