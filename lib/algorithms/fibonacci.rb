# fibonacci.rb
# 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144.


class Fibonacci
  def calculate(n)
    return 1 if n <= 2
    calculate(n - 1) + calculate(n - 2)
  end

  def calculate_mem(n, memoize = {1 => 1, 2 => 1})
    if memoize.key?(n)
      return memoize[n]
    else
      memoize[n] = calculate_mem(n - 1, memoize) + calculate_mem(n - 2, memoize)
      return memoize[n]
    end
  end

  def calculate_iter(n)
    last_two = [1,1]
    counter = 3
    while counter <= n
      next_fib = last_two[0] + last_two[1]
      last_two[0] = last_two[1]
      last_two[1] = next_fib
      counter += 1
    end
    return last_two[1] if n > 1
    last_two[0]
  end
end

# 20.times do |n|
#   puts Fibonacci.new.calculate(n)
# end