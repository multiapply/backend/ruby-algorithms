class Node
  attr_accessor :value, :next
  def initialize(value, next_node = nil)
    @value = value
    @next = next_node
  end
end

class Stack
  def initialize
    @top = nil
    @bottom = nil
    @length = 0
  end

  def peek
    @top
  end

  def list_nodes
    values = []
    current_node = @top
    @length.times do
      values.push(current_node.value)
      current_node = current_node.next
    end
    values
  end

  def push(value)
    new_node = Node.new(value)
    if @length == 0
      @top = new_node
      @bottom = new_node
    else
      holding_pointer = @top
      @top = new_node
      @top.next = holding_pointer
    end
    @length += 1
    @top
  end

  def pop
    top_value = @top.value
    @top = @top.next
    @length -= 1
    top_value
  end

end