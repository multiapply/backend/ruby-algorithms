class CustomArray
  def initialize
    @length = 0
    @data = {}
  end

  def get(index)
    @data[index]
  end

  def push(item)
    @data[@length] = item
    @length += 1
    @data
  end

  def pop
    last_index = @length - 1
    last_item = @data[last_index]
    @data.delete(last_index)
    @length -= 1
    [@data, last_item]
  end

  def delete(index)
    item = @data[index]
    shift_left_from(index)
    [@data, item]
  end

  def shift_left_from(index)
    last_index = @length - 1
    (index..last_index).each do |idx|
      @data[idx] = @data[idx+1]
    end
    @data.delete(last_index)
    @length -= 1
  end

  # push inverse
  def add_start(item)
    make_start_space
    @data[0] = item
    @data
  end
  
  def make_start_space
    last_index = @length - 1
    last_index.downto(0) do |idx|
      @data[idx+1] = @data[idx]
    end
    @data[0] = ""
    @length +=1
  end

  def pop_start
    first_item = @data[0]
    delete(0)
    [@data, first_item]
  end

  def show
    @data
  end
end