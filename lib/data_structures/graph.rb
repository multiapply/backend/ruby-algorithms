class Graph
  attr_reader :adjacent_list
  def initialize
    @nodes = 0
    @adjacent_list = {}
  end

  def add_vertex(node)
    @adjacent_list[node] = []
    @nodes += 1
  end

  def add_edge(node1, node2)
    @adjacent_list[node1].push(node2)
    @adjacent_list[node2].push(node1)
  end
end