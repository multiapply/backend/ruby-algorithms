class Node
  attr_accessor :value, :next
  def initialize(value, next_node = nil)
    @value = value
    @next = next_node
  end
end

class Queue
  def initialize
    @first = nil
    @last = nil
    @length = 0
  end

  def peek
    @first
  end

  def list_nodes
    values = []
    current_node = @first
    @length.times do
      values.push(current_node.value)
      current_node = current_node.next
    end
    values
  end

  def enqueue(value)
    new_node = Node.new(value)
    if @length == 0
      @first = new_node
      @last = new_node
    else
      @last.next = new_node
      @last = new_node
    end
    @length += 1
    @first
  end

  def dequeue
    out_value = @first.value
    @first = @first.next
    @length -= 1
    out_value
  end
end