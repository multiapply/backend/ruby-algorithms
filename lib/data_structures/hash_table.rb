class HashTable
  def initialize(size)
    @data = Array.new(size)
  end

  def hash_method(key)
    hash = 0
    (0...key.length).each do |idx|
      hash = (hash + key[idx].ord * idx) % @data.length
    end
    hash
  end
  
  def set(key, value)
    address  = hash_method(key)
    unless @data[address]
        @data[address] = []
    end
    @data[address].push([key, value])
    @data
  end

  def get(key)
    address = hash_method(key)
    current_bucket = @data[address]

    if current_bucket
      (0...current_bucket.length).each do |idx|
        if current_bucket[idx][0].eql? key
          return current_bucket[idx][1]
        end
      end
    end
    "no value"
  end

  def remove(key)
    address = hash_method(key)
    current_bucket = @data[address]
    if current_bucket
      (0...current_bucket.length).each do |idx|
        if current_bucket[idx][0].eql? key
          deleted_record = current_bucket[idx]
          current_bucket.delete_at(idx)
          return deleted_record
        end
      end
    end
    "no value"
  end
  
  def keys
    list_keys = []
    @data.each do |bucket|
      unless bucket.nil?
        bucket.each do |element|
          list_keys.push(element[0])
        end
      end
    end
    list_keys
  end
end