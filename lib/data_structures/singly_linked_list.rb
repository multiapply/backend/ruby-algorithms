class SinglyLinkedList
  def initialize
    @head = nil
    @tail = @head
    @length = 0
  end

  def append(value)
    new_node = Node.new(value)
    if @head.nil?
      @head = new_node
    else
      @tail.next = new_node
    end
    @tail = new_node

    @length +=1
    @head
  end

  def prepend(value)
    new_node = Node.new(value, @head)

    @head = new_node
    @length += 1
    @head
  end

  def insert(index,value)
    return append(value) if index >= @length

    new_node = Node.new(value)
    first_pointer = get_index(index-1)
    holding_pointer = first_pointer.next
    
    first_pointer.next = new_node
    new_node.next = holding_pointer

    @length += 1
    @head
  end

  def get_index(index)
    counter = 0
    current_node = @head
    until counter.eql? index
      current_node = current_node.next
      counter += 1
    end
    current_node
  end

  def remove(index)
    return "doesnt exists" if index >= @length

    first_pointer = get_index(index-1)
    holding_pointer = first_pointer.next.next

    first_pointer.next = holding_pointer

    @length -= 1
    @head
  end

  def list_nodes
    values = []
    current_node = @head
    @length.times do
      values.push(current_node.value)
      # puts current_node.value
      current_node = current_node.next
    end
    values
  end
end


class Node
  attr_accessor :value, :next
  def initialize(value, next_node = nil)
    @value = value
    @next = next_node
  end
end