class Node
  attr_accessor :value, :left, :right
  def initialize(value, left_node = nil, right_node = nil)
    @value = value
    @left = left_node
    @right = right_node
  end
end

class BinarySearchTree
  def initialize
    @root = nil
  end

  def list_pre_order(values = [], node = @root)
    if node != nil
      values.push(node.value)
      values = list_pre_order(values, node.left)
      values = list_pre_order(values, node.right)
    end
    values
  end

  def list_pre_order_pos(values = [], node = @root, parent = "/")
    if node != nil
      values.push("#{parent}: #{node.value}")
      values = list_pre_order_pos(values, node.left, "L")
      values = list_pre_order_pos(values, node.right, "R")
    end
    values
  end

  def insert(value)
    value = value.to_i
    new_node = Node.new(value)
    if @root.nil?
      @root = new_node
    else
      current_node = @root
      while true
        if value < current_node.value
          if current_node.left.nil?
            current_node.left = new_node
            return @root
          end
          current_node = current_node.left
        else
          if current_node.right.nil?
            current_node.right = new_node
            return @root
          end
          current_node = current_node.right
        end
      end
    end
    @root
  end
end