require 'thor'
require_relative 'commands/algorithm_command'
require_relative 'commands/data_structure_command'

module Ralgo
  class CLI < Thor
    desc "algo", "Runs an Algorithm"
    subcommand "algo", Commands::AlgorithmCommand

    desc "ds", "Builds a DataStructure"
    subcommand "ds", Commands::DataStructureCommand
    
    desc "start", "Start the Ralgo REPL"
    def start
      Ralgo::REPL.new.run
    end
  end
end