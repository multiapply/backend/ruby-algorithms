require "./lib/algorithms/fizzbuzz.rb"
require 'highline/import'

module Ralgo
  module Algorithms
    class FizzBuzzAlgo < AlgoCommands::Base
      def name
        "FizzBuzz"
      end

      def execute
        puts "Executing FizzBuzz"
        number = ask("Enter a number: ")
        fizzbuzz = FizzBuzz.new
        result = fizzbuzz.convert(number.to_i)
        puts "FizzBuzz for #{number} is #{result}!"
      end
    end
  end
end
