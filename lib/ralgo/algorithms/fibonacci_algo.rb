require "./lib/algorithms/fibonacci.rb"
require 'highline/import'

module Ralgo
  module Algorithms
    class FibonacciAlgo < AlgoCommands::Base
      def name
        "Fibonacci"
      end

      def execute
        puts "Executing Fibonacci"
        number = ask("Enter a number: ")
        fibonacci = Fibonacci.new
        result = fibonacci.calculate(number.to_i)
        puts "Fibonacci for #{number} is #{result}!"
      end
    end
  end
end
