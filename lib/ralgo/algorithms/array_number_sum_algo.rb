require "./lib/algorithms/array_number_sum.rb"
require 'highline/import'
require 'set'

module Ralgo
  module Algorithms
    class ArrayNumberSumAlgo < AlgoCommands::Base
      def name
        "ArrayNumberSum"
      end

      def execute
        puts "Executing ArrayNumberSum"
        # numbers_to_sum = ask("How many numbers to sum? (2): ") { |q| q.default = 2 }
        numbers_to_sum = 2
        array_size = ask("How many numbers in array? (8): ", Integer) { |q| q.default = 8 }
        array_generated = generate_random_integers(array_size,-10,10)
        puts array_generated.inspect

        target = ask("Choose a Target Sum: ").to_i
        ans = ArrayNumberSum.new
        if numbers_to_sum == 2
          result = ans.two_number_sum_pointers(array_generated, target)
          puts "the 2 numbers are #{result}!"
        else
          puts "only 2 number sum is supported"
        end

      end

      # def generate_random_integers(count, min_value = -100, max_value = 100)
      #   Array.new(count) { rand(min_value..max_value) }
      # end
      

      def generate_random_integers(count, min_value = -100, max_value = 100)
        raise ArgumentError, "Range is too small for the count of unique numbers requested" if (max_value - min_value + 1) < count
        
        unique_numbers = Set.new
        while unique_numbers.size < count
          unique_numbers << rand(min_value..max_value)
        end
        
        unique_numbers.to_a
      end
    end
  end
end
