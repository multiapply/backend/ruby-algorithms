require "./lib/algorithms/01_strings/run_length_encoding.rb"
require 'highline/import'

module Ralgo
  module Algorithms
    class RunLengthEncode < AlgoCommands::Base
      def name
        "Strings -> RunLengthEncoding"
      end

      def execute
        puts "Executing RunLengthEncoding"
        target_string = ask("Enter a string to encode?: ", String)
        rle = RunLengthEncoding.new
        result = rle.encode_str(target_string)
        puts "#{target_string} encoded is: #{result}!"

      end
    end
  end
end
