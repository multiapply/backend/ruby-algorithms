require "./lib/algorithms/01_strings/generate_document.rb"
require 'highline/import'

module Ralgo
  module Algorithms
    class GenerateDocumentAlgo < AlgoCommands::Base
      def name
        "Strings -> GenerateDocument"
      end

      def execute
        puts "Executing GenerateDocument"
        characters = ask("Enter a list of characters: ", String)
        document = ask("Enter the text document to validate: ", String)
        gd = GenerateDocument.new
        result = gd.validate_with_hash(characters, document)
        if result
          puts "#{document} can be build from #{characters} characters"
        else
          puts "cannot build document from given characters"
        end
      end
    end
  end
end
