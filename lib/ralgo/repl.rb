require 'highline/import'

module Ralgo
  class REPL
    def initialize
      @running = true

      @categories = {
        'algo' => load_commands(Ralgo::Algorithms),
        'ds' => load_commands(Ralgo::DataStructures)
      }
    end

    def run
      puts "Welcome to Ralgo! Type 'exit' to quit."
      while @running
        print_categories
        input = ask("> ")

        handle_input(input)
      end
    end

    private

    def load_commands(namespace)
      namespace.constants.map { |constant| namespace.const_get(constant).new }
    end

    def print_categories
      puts "\nAvailable Categories:"
      @categories.keys.each { |key| puts key.capitalize }
    end


    def handle_input(input)
      case input
      when 'exit'
        @running = false
        puts "Goodbye!"
      else
        handle_category_selection(input.downcase)
      end
    end

    def handle_category_selection(category)
      if @categories.key?(category)
        commands = @categories[category]
        print_commands(commands)
        input = ask("> ", Integer).to_i
        handle_command_selection(commands, input)
      else
        puts "Invalid category, please try again."
      end
    end

    def print_commands(commands)
      puts "\nAvailable Commands:"
      commands.each_with_index do |command, index|
        puts "#{index + 1}. #{command.name}"
      end
    end

    def handle_command_selection(commands, choice)
      command = commands[choice - 1]
      if command
        command.execute
      else
        puts "Invalid choice, please try again."
      end
    end
  end
end
