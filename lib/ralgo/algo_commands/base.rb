module Ralgo
  module AlgoCommands
    class Base
      def name
        raise NotImplementedError, "This method should be overridden in subclasses"
      end

      def execute
        raise NotImplementedError, "This method should be overridden in subclasses"
      end
    end
  end
end
