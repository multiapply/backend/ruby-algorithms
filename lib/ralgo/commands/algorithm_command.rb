require 'thor'
require "./lib/algorithms/fizzbuzz.rb"

module Ralgo
  module Commands
    class AlgorithmCommand < Thor
      desc "fizzbuzz Number", "Execute FizzBuzz algo given a number"
      def fizzbuzz(number)
        fizzbuzz = FizzBuzz.new
        result = fizzbuzz.convert(number.to_i)
        puts "FizzBuzz for #{number} is #{result}!"
      end
    end
  end
end