
require 'thor'
require 'highline/import'

module Ralgo
  module Commands
    class DataStructureCommand < Thor
      desc "array", "Build an array with certain name"
      def array
        name = ask("What is the array name?")
        puts "Lets build an array for #{name}!"
      end
      
      desc "linked_list", "Build an linked_list with certain name"
      def linked_list
        name = ask("What is the linked_list name?")
        puts "Lets build an linked_list for #{name}!"
      end
    end
  end
end