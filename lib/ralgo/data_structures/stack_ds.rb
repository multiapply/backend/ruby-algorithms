require "./lib/data_structures/stack.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class StackDs< AlgoCommands::Base
      def name
        "Stack"
      end

      def execute
        puts "Executing Stack Operations"
        stack = Stack.new
        loop do
          input = ask("Enter command (push <value>/pop): ").split
          command = input[0]
          case command
          when 'push'
            value = input[1]

            stack.push(value)
            puts "Nodes : #{stack.list_nodes}"
          when 'pop'
            value = stack.pop
            puts "popped value : #{value}"
            puts "Nodes : #{stack.list_nodes}"
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
