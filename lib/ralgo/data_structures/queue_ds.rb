require "./lib/data_structures/queue.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class QueueDs< AlgoCommands::Base
      def name
        "Queue"
      end

      def execute
        puts "Executing Queue Operations"
        queue = Queue.new
        loop do
          input = ask("Enter command (enq <value>/deq): ").split
          command = input[0]
          case command
          when 'enq'
            value = input[1]

            queue.enqueue(value)
            puts "Nodes : #{queue.list_nodes}"
          when 'deq'
            out_value = queue.dequeue
            puts "Dequeued : #{out_value}"
            puts "Nodes : #{queue.list_nodes}"
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
