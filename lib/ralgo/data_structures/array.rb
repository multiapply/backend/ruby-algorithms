require "./lib/data_structures/custom_array.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class Array < AlgoCommands::Base
      def name
        "Custom Array"
      end

      def execute
        puts "Executing Custom Array Operations"
        cusArray = CustomArray.new
        loop do
          input = ask("Enter command (show/push <value>/ipush <value>/get <index>/del <index>/pop/ipop/exit): ").split
          command = input[0]
          case command
          when 'show'
            currArray = cusArray.show
            puts "Current : #{currArray}"
          when 'push'
            value = input[1]
            addedItem = cusArray.push(value)
            puts "Added: #{addedItem}"
          when 'pop'
            currArray, removedItem = cusArray.pop
            puts "Removed: #{removedItem}"
            puts "#{currArray}"
          when 'get'
            index = input[1].to_i
            value = cusArray.get(index)
            puts "Index: #{index}, Value: #{value}"
          when 'del'
            index = input[1].to_i
            currArray, removedItem = cusArray.delete(index)
            puts "Removed: #{index} #{removedItem}"
            puts "#{currArray}"
          when 'ipush'
            value = input[1]
            addedItem = cusArray.add_start(value)
            puts "Added: #{addedItem}"
          when 'ipop'
            currArray, removedItem = cusArray.pop_start
            puts "Removed: #{removedItem}"
            puts "#{currArray}"
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
