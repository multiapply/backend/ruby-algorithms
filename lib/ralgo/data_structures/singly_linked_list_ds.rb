require "./lib/data_structures/singly_linked_list.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class SinglyLinkedListDs< AlgoCommands::Base
      def name
        "Singly Linked List"
      end

      def execute
        puts "Executing Singly Linked List Operations"
        singly_linked_list = SinglyLinkedList.new
        loop do
          input = ask("Enter command (append <value>/prepend <value>/insert <idx> <value>/remove <idx>): ").split
          command = input[0]
          case command
          when 'append'
            value = input[1]

            currList = singly_linked_list.append(value)
            puts "Nodes : #{singly_linked_list.list_nodes}"
          when 'prepend'
            value = input[1]

            currList = singly_linked_list.prepend(value)
            puts "Nodes : #{singly_linked_list.list_nodes}"
          when 'insert'
            index = input[1].to_i
            value = input[2]

            currList = singly_linked_list.insert(index, value)
            puts "Nodes : #{singly_linked_list.list_nodes}"
          when 'remove'
            index = input[1].to_i

            currList = singly_linked_list.remove(index)
            puts "Nodes : #{singly_linked_list.list_nodes}"
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
