require "./lib/data_structures/hash_table.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class HashTableDs < AlgoCommands::Base
      def name
        "Hash Table"
      end

      def execute
        puts "Executing Hash Table Operations"
        hash_table = HashTable.new(50)
        loop do
          input = ask("Enter command (set <key> <value>/get <key>/del <key>/keys): ").split
          command = input[0]
          case command
          when 'set'
            key = input[1]
            value = input[2]

            currHash = hash_table.set(key, value)
            puts "Current : #{currHash}"
          when 'get'
            key = input[1]

            value = hash_table.get(key)
            puts "Value : #{value}"
          when 'del'
            key = input[1]

            value = hash_table.remove(key)
            puts "Removed : #{value}"
          when 'keys'
            hash_table.keys.each do |key|
              puts "- #{key}"
            end
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
