require "./lib/data_structures/binary_search_tree.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class BinarySearchTreeDs< AlgoCommands::Base
      def name
        "BinarySearchTree"
      end

      def execute
        puts "Executing BinarySearchTree Operations"
        bst = BinarySearchTree.new
        loop do
          input = ask("Enter command (insert <value>): ").split
          command = input[0]
          case command
          when 'insert'
            value = input[1]

            bst.insert(value)
            puts "Nodes : #{bst.list_pre_order}"
            puts "Nodes : #{bst.list_pre_order_pos}"
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
