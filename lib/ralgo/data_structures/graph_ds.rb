require "./lib/data_structures/graph.rb"
require 'highline/import'

module Ralgo
  module DataStructures
    class GraphDs< AlgoCommands::Base
      def name
        "Graph"
      end

      def execute
        puts "Executing Graph Operations"
        graph = Graph.new
        loop do
          input = ask("Enter command (vertex <value>/edge <value> <value>): ").split
          command = input[0]
          case command
          when 'vertex'
            value = input[1]

            graph.add_vertex(value)
            puts "Nodes : #{graph.adjacent_list}"
          when 'edge'
            node1 = input[1]
            node2 = input[2]

            graph.add_edge(node1,node2)
            puts "Nodes : #{graph.adjacent_list}"
          when 'exit'
            break
          else
            puts "Unknown command"
          end

        end
      end

    end
  end
end
