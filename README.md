
## Install & Run
```ruby
# Install
bundle install
# Test
ruby test/fizzbuzz_test.rb
# Run
ruby bin/ralgo
ruby bin/ralgo start
ruby bin/ralgo algo fizzbuzz 3
ruby bin/ralgo ds linked_list

```