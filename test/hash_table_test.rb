require "minitest/autorun"
require "./lib/data_structures/hash_table.rb"

class HashTableTest < Minitest::Test
  def setup
    @hash = HashTable.new(50)
  end

  def test_set_keys
    @hash.set("diego",1990)
    local_hash = @hash.set("mariana",1998)
    local_address = @hash.hash_method("mariana")
    # p local_hash
    # p local_address

    assert_equal 2, local_hash[local_address].length
  end

  def test_get_key
    @hash.set("diego",1990)
    local_hash = @hash.set("mariana",1998)
    value = @hash.get("mariana")
    
    assert_equal 1998, value
  end

  def test_remove_element
    @hash.set("diego",1990)
    local_hash = @hash.set("mariana",1998)
    record = @hash.remove("mariana")
    
    assert_equal "no value", @hash.get("mariana")
  end

  def test_get_all_keys
    @hash.set("diego",1990)
    @hash.set("mariana",1998)
    @hash.set("mario",2002)
    keys = @hash.keys
    assert_equal 3, keys.count
    assert_includes keys, "diego"
    assert_includes keys, "mariana"
  end

end