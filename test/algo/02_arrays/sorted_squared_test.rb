require "minitest/autorun"

require "./lib/algorithms/02_arrays/sorted_squared.rb"

class SortedSquaredTest < Minitest::Test
  def setup
    @ssa = SortedSquared.new
  end

  def test_array_is_sorted_and_squared
    array = [1,2,3,5,6,8,9]
    expected = [1,4,9,25,36,64,81]
    result = @ssa.sort_and_square(array)
    assert_equal expected, result
  end
end