require "minitest/autorun"

require "./lib/algorithms/02_arrays/validate_subsequence.rb"

class ValidateSubsequenceTest < Minitest::Test
  def setup
    @vss = ValidateSubsequence.new
  end

  def test_sequence_is_subsequence_of_array
    array = [5,1,22,25,6,-1,8,10]
    sequence = [1,6,-1,10]
    result = @vss.validate(array,sequence)
    assert_equal true, result
  end

  def test_sequence_is_subsequence_of_array_for
    array = [5,1,22,25,6,-1,8,10]
    sequence = [1,6,-1,10]
    result = @vss.validate_with_for(array,sequence)
    assert_equal true, result
  end
end