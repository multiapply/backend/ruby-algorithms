require "minitest/autorun"

require "./lib/algorithms/01_strings/caesar_cipher.rb"

class CaesarCipherTest < Minitest::Test
  def setup
    @cc = CaesarCipher.new
  end

  def test_encrypt_with_key_2
    text = "xyz"
    enc_key = 2
    enc_result = @cc.encrypt(text,enc_key)
    assert_equal "zab", enc_result
  end
end