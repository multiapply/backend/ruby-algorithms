require 'minitest/autorun'
require './lib/algorithms/01_strings/palindrome_check.rb'

class PalindromeCheckTest < Minitest::Test
  def setup
    @pc = PalindromeCheck.new
  end

  def test_palindrome_string
    target = "abcdcba"
    result = @pc.check(target)
    assert_equal true, result
  end

  def test_palindrome_arr
    target = "abcdcba"
    result = @pc.check_with_arr(target)
    assert_equal true, result
  end

  def test_palindrome_recur
    target = "abcdcba"
    result = @pc.check_with_recur(target)
    assert_equal true, result
  end

  def test_palindrome_tail_recur
    target = "abcdcba"
    result = @pc.check_with_tail_recur(target)
    assert_equal true, result
  end

  def test_palindrome_pointers
    target = "abcdcba"
    result = @pc.check_with_pointers(target)
    assert_equal true, result
  end

end