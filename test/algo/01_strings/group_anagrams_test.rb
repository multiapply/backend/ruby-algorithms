require "minitest/autorun"

require "./lib/algorithms/01_strings/group_anagrams.rb"

class GroupAnagramsTest < Minitest::Test
  def setup
    @groupper = GroupAnagrams.new
  end

  def test_group_words
    words = %w[yo act flop tac cat oy olfp]
    groupped = [ %w[act tac cat],%w[flop olfp], %w[yo oy]]
    result = @groupper.group(words)
    assert_equal groupped, result
  end
end