require "minitest/autorun"

require "./lib/algorithms/01_strings/longest_palindromic_substring.rb"

class LongestPalindromicSubstringTest < Minitest::Test
  def setup
    @lps = LongestPalindromicSubstring.new
  end

  def test_find_longest_palindrome
    text = 'abaxyzzyxf'
    longest = 'xyzzyx'
    found_result = @lps.find_longest(text)
    assert_equal longest, found_result
  end
end