require "minitest/autorun"
require "./lib/algorithms/01_strings/run_length_encoding.rb"

class RunLengthEncodingTest < Minitest::Test
  def setup
    @rle = RunLengthEncoding.new
  end

  def test_encode_string
    plain_string = 'AAAAAAAAAAAAABBCCCCDD'
    encoded = @rle.encode_str(plain_string)
    assert_equal '9A4A2B4C2D', encoded
  end
end
