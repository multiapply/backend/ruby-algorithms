require "minitest/autorun"
require "./lib/algorithms/01_strings/first_non_repeat_char.rb"


class FirstNonRepeatTest < Minitest::Test
  def setup
    @fnr = FirstNonRepeatChar.new
  end

  def test_find_first_non_repeating_char
    text = "abcdcaf"
    index = @fnr.first_non_repeating_character(text)
    assert_equal 1, index
  end

  def test_dont_find_first_non_repeating_char
    text = "aaabbccc"
    index = @fnr.first_non_repeating_character(text)
    assert_equal -1, index
  end

  def test_find_first_non_repeating_char_hash_tracked
    text = "abcdcaf"
    index = @fnr.first_non_repeating_char_hash(text)
    assert_equal 1, index
  end

  def test_dont_find_first_non_repeating_char_hash_tracked
    text = "aaabbccc"
    index = @fnr.first_non_repeating_char_hash(text)
    assert_equal -1, index
  end
end