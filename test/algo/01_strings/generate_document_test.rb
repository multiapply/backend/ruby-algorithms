require "minitest/autorun"
require "./lib/algorithms/01_strings/generate_document.rb"

class GenerateDocumentTest < Minitest::Test
  def setup
    @gd = GenerateDocument.new
  end

  def test_validate_document_generation
    characters = "_i_isemTsg_sa!apssMehla_e"
    document = "This_is_a_sample_Message!"
    result = @gd.validate_per_char(characters,document)
    assert_equal true, result
  end

  def test_validate_document_generation_uniq
    characters = "_i_isemTsg_sa!apssMehla_e"
    document = "This_is_a_sample_Message!"
    result = @gd.validate_per_char_unique(characters,document)
    assert_equal true, result
  end

  def test_validate_document_generation_with_hash
    characters = "_i_isemTsg_sa!apssMehla_e"
    document = "This_is_a_sample_Message!"
    result = @gd.validate_with_hash(characters,document)
    assert_equal true, result
  end

end