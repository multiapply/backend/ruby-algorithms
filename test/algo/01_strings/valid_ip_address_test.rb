require "minitest/autorun"

require "./lib/algorithms/01_strings/valid_ip_address"

class ValidIPAddressTest < Minitest::Test
  def setup
    @validIp = ValidIPAddress.new
  end

  def test_generates_valid_ip_addresses
    ip_address = "1921680"
    posibilities = @validIp.generate_valid_addresses(ip_address)
    assert_equal 11, posibilities.length
  end
end