require "minitest/autorun"
require "./lib/data_structures/graph.rb"

class GraphTest < Minitest::Test
  def setup
    @graph = Graph.new
  end

  def test_add_vertex_node
    @graph.add_vertex("1")
    @graph.add_vertex("3")
    assert_equal [], @graph.adjacent_list["1"]
    assert_equal [], @graph.adjacent_list["3"]
  end

  def test_add_edge_between_nodes
    @graph.add_vertex("1")
    @graph.add_vertex("3")
    @graph.add_vertex("4")
    @graph.add_vertex("5")
    @graph.add_vertex("6")
    @graph.add_vertex("8")
    @graph.add_edge("1","6")
    @graph.add_edge("1","3")
    @graph.add_edge("1","4")
    @graph.add_edge("6","3")
    @graph.add_edge("3","5")
    @graph.add_edge("4","5")
    @graph.add_edge("4","8")
    assert_equal ["6","3","4"], @graph.adjacent_list["1"]
    assert_equal ["1","5","8"], @graph.adjacent_list["4"]
  end
end