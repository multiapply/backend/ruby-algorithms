require "minitest/autorun"
require "./lib/data_structures/stack.rb"

class StackTest < Minitest::Test
  def setup
    @stack = Stack.new
  end

  def test_push_value
    @stack.push("first")
    @stack.push("second")
    assert_equal "second", @stack.peek.value
    assert_equal "first", @stack.peek.next.value
  end

  def test_list_values
    @stack.push("first")
    @stack.push("second")
    @stack.push("third")
    nodes = @stack.list_nodes
    assert_equal "third", nodes.first
    assert_equal "first", nodes.last
  end

  def test_pop_value
    @stack.push("first")
    @stack.push("second")
    @stack.push("third")
    popped_value = @stack.pop
    assert_equal "third", popped_value
    assert_equal "second", @stack.peek.value
    assert_equal "first", @stack.peek.next.value
  end
end