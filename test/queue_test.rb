require "minitest/autorun"
require "./lib/data_structures/queue.rb"

class QueueTest < Minitest::Test
  def setup
    @queue = Queue.new
  end

  def test_enqueue_value
    @queue.enqueue("first")
    @queue.enqueue("second")
    assert_equal "first", @queue.peek.value
    assert_equal "second", @queue.peek.next.value
  end

  def test_list_values
    @queue.enqueue("first")
    @queue.enqueue("second")
    @queue.enqueue("third")
    nodes = @queue.list_nodes
    assert_equal "first", nodes.first
    assert_equal "third", nodes.last
  end

  def test_dequeue_value
    @queue.enqueue("first")
    @queue.enqueue("second")
    out_value = @queue.dequeue
    assert_equal "first", out_value
    assert_equal "second", @queue.peek.value
  end

end