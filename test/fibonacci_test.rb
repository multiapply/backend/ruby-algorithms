require "minitest/autorun"
require "./lib/algorithms/fibonacci.rb"

class FibonacciTest < Minitest::Test
  def setup
    @fibonacci = Fibonacci.new
  end

  def test_fibonnaci_12
    assert_equal 144, @fibonacci.calculate(12)
  end

  def test_fibonnaci_1
    assert_equal 1, @fibonacci.calculate(1)
  end

  def test_fibonnaci_12_mem
    assert_equal 144, @fibonacci.calculate_mem(12)
  end

  def test_fibonnaci_1_mem
    assert_equal 1, @fibonacci.calculate_mem(1)
  end

  def test_fibonnaci_12_iter
    assert_equal 144, @fibonacci.calculate_iter(12)
  end

  def test_fibonnaci_1_iter
    assert_equal 1, @fibonacci.calculate_iter(1)
  end

end