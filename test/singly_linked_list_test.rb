require "minitest/autorun"
require "./lib/data_structures/singly_linked_list.rb"

class SinglyLinkedListTest < Minitest::Test
  def setup
    @singly = SinglyLinkedList.new
  end

  def test_append_value
    @singly.append("first")
    linked_list = @singly.append("second")
    assert_equal "first", linked_list.value
    assert_equal "second", linked_list.next.value
  end

  def test_append_two_values
    @singly.append("first")
    @singly.append("second")
    linked_list = @singly.append("third")
    assert_equal "first", linked_list.value
    assert_equal "second", linked_list.next.value
    assert_equal "third", linked_list.next.next.value
  end

  def test_prepend_value
    @singly.append("first")
    linked_list = @singly.prepend("second")
    assert_equal "second", linked_list.value
    assert_equal "first", linked_list.next.value
  end

  def test_insert_value
    @singly.append("first")
    @singly.append("second")
    @singly.append("third")
    linked_list = @singly.insert(1,"middle")
    assert_equal "first", linked_list.value
    assert_equal "middle", linked_list.next.value
    assert_equal "second", linked_list.next.next.value
  end

  def test_remove_value
    @singly.append("first")
    @singly.append("second")
    @singly.append("third")
    linked_list = @singly.remove(1)
    assert_equal "first", linked_list.value
    assert_equal "third", linked_list.next.value
  end

  def test_list_values
    @singly.append("first")
    @singly.append("second")
    @singly.append("third")
    nodes = @singly.list_nodes
    assert_equal "first", nodes.first
    assert_equal "third", nodes.last
  end
end