require "minitest/autorun"
require "./lib/algorithms/array_number_sum.rb"

class ArrayNumberSumTest < Minitest::Test
  def setup
    @ans = ArrayNumberSum.new
  end

  def test_two_number_sum_10
    arr = [3,5,-4,8,11,1,6,-1]
    assert_equal [11,-1], @ans.two_number_sum(arr,10)
  end

  def test_two_number_sum_10_with_hash
    arr = [3,5,-4,8,11,1,6,-1]
    assert_equal [11,-1], @ans.two_number_sum_hash(arr,10)
  end

  def test_two_number_sum_10_with_two_pointers
    arr = [3,5,-4,8,11,1,6,-1]
    assert_equal [-1,11], @ans.two_number_sum_pointers(arr,10)
  end

end