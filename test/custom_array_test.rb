require "minitest/autorun"
require "./lib/data_structures/custom_array.rb"

class CustomArrayTest < Minitest::Test
  def setup
    @cusArray = CustomArray.new
  end

  def test_push
    @cusArray.push("Mario")
    resultArray = @cusArray.push("Markus")

    assert_equal 2, resultArray.length
    assert_equal "Markus", @cusArray.get(1)
  end

  def test_pop
    @cusArray.push("Mario")
    @cusArray.push("Markus")

    _ , poppedItem = @cusArray.pop

    assert_equal "Markus", poppedItem
    assert_nil @cusArray.get(1)
  end

  def test_delete
    @cusArray.push("Mario")
    @cusArray.push("Markus")
    resultArray = @cusArray.push("Maria")
    @cusArray.delete(1)

    assert_equal 2, resultArray.length
    assert_nil @cusArray.get(2)
  end

  def test_add_start
    @cusArray.push("Mario")
    @cusArray.push("Markus")
    resultArray = @cusArray.add_start("Maria")

    assert_equal 3, resultArray.length
    assert_equal "Maria", @cusArray.get(0)
  end

  def test_pop_start
    @cusArray.push("Mario")
    @cusArray.push("Markus")

    _ , poppedItem = @cusArray.pop_start

    assert_equal "Mario", poppedItem
    assert_nil @cusArray.get(1)
  end

end