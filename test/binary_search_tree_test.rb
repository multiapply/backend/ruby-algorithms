require "minitest/autorun"
require "./lib/data_structures/binary_search_tree.rb"

class BinarySearchTreeTest < Minitest::Test
  def setup
    @bst = BinarySearchTree.new
  end

  def test_insert_root_value
    root = @bst.insert(10)
    assert_equal 10, root.value
  end

  def test_insert_value
    @bst.insert(10)
    @bst.insert(4)
    root = @bst.insert(20)
    assert_equal 10, root.value
    assert_equal 4, root.left.value
    assert_equal 20, root.right.value
  end

  def test_list_pre_order
    @bst.insert(10)
    @bst.insert(4)
    @bst.insert(20)
    values = @bst.list_pre_order

    assert_equal 10, values.first
    assert_equal 4, values[1]
    assert_equal 20, values.last
  end

  def test_list_pre_order_pos
    @bst.insert(10)
    @bst.insert(4)
    @bst.insert(20)
    values = @bst.list_pre_order_pos

    assert_equal "/: 10", values.first
    assert_equal "L: 4", values[1]
    assert_equal "R: 20", values.last
  end
end